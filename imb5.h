/*
project properties:

All configurations
	VC++ directories
		Include directories: openssl-win64\include;$(IncludePath)
		Library directories: openssl-win64\lib\VC\static;$(LibraryPath)
	C++
		C++ language standard: ISO C++17 Standard (/std:c++17)
		Pre compiled headers: Not Using Precompiled Headers
Debug
	Linker
		Additional dependencies: legacy_stdio_definitions.lib;libeay32MTd.lib;ssleay32MTd.lib;%(AdditionalDependencies)

Release
	Linker
		Additional dependencies: legacy_stdio_definitions.lib;libeay32MT.lib;ssleay32MT.lib;%(AdditionalDependencies)

*/


#pragma once

#include <limits>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <cstdint>
#include <thread>
#include <mutex>
#include <shared_mutex>
#include <atomic>
#include <memory>


// platform dependent code: start
#ifdef __linux__

#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>

typedef int SOCKET;

#include <termios.h>


typedef struct _GUID {
	unsigned long  Data1;
	unsigned short Data2;
	unsigned short Data3;
	unsigned char  Data4[8];
} GUID;

#ifdef NDEBUG
#define DEBUG(x) {}
#else
#include <iostream>
#define DEBUG(x) do { std::cerr << x << std::endl; } while (0)
#endif

#elif _WIN32

#include <stdio.h>
#include <iostream>
#include <cstdint>
#include <winsock2.h>
#include <conio.h>

// winsock2
#pragma comment(lib, "Rpcrt4.lib")
#pragma comment(lib, "Ws2_32.lib")

#include <WS2tcpip.h>

// ssl
#include <openssl/ssl.h>
#include <openssl/err.h>

#define DEBUG(x) OutputDebugStringA(x)

extern "C" { FILE __iob_func[3] = { *stdin,*stdout,*stderr }; }

// initialize en finalize winsock
class WinsockInitialization {
public:
	WinsockInitialization(void) {
		// initialize winsock
		WSADATA wsaData;
		int res = WSAStartup(MAKEWORD(2, 2), &wsaData);
		if (res != 0)
			DEBUG(("## Could not initialize winsock in WinsockInitialization::WinsockInitialization: " + std::to_string(res)).c_str());
	};
	~WinsockInitialization(void) {
		// clean up winsock
		WSACleanup();
	};
};

WinsockInitialization winsockInitialization;

#define SHUT_RDWR SD_BOTH 

int close(SOCKET s) {
	return closesocket(s);
}

#endif
// platform dependent code: end

// initialize ssl

SOCKET connect_tcp_socket(const std::string & aHost, const std::string & aPort);
SOCKET create_tcp_socket(int port);
SSL_CTX *create_context();
void configure_context(SSL_CTX *ctx);

class SSLInitialization {
public:
	SSLInitialization(void) {
		// init ssl
		SSL_load_error_strings();
		OpenSSL_add_ssl_algorithms();
		fCTX = create_context();
		configure_context(fCTX);
	};
	~SSLInitialization(void) {
		SSL_CTX_free(fCTX);
		EVP_cleanup();
		fCTX = 0;
	};
	SSL_CTX * CTX(){ return fCTX; };
private:
	SSL_CTX * fCTX;
};

SSLInitialization sslInitialization;

// time (in file time format)
uint64_t NowUTCInt64();

typedef unsigned char byte;

typedef uint32_t TEventID;
typedef uint16_t TEventIDFixed;

#define imbEventFilterPostFix "*"
#define imbEventNameSeperator "."
#define imbMonitorEventNamePrefix "Hubs"
#define imbMonitorEventNamePostfix "Monitor"

// protobuf wire types
const int wtVarInt = 0;												// int32, int64, uint32, uint64_t, sint32, sint64, bool, enum
const int wt64Bit = 1;												// double or fixed int64/uint64_t
const int wtLengthDelimited = 2;									// std::string, bytes, embedded messages, packed repeated fields
const int wtStartGroup = 3;											// deprecated
const int wtEndGroup = 4;											// deprecated
const int wt32Bit = 5;												// float (single) or fixed int32/uint32

const int imbMaxStreamBodyBuffer = 8 * 1024;

// change object actions
const int actionNew = 0;
const int actionDelete = 1;
const int actionChange = 2;

// basic event tags
const int icehIntString = 1;										// <varint>
const int icehIntStringPayload = 2;									// <std::string>
const int icehString = 3;											// <std::string>
const int icehChangeObject = 4;										// <int32: varint>
const int icehChangeObjectAction = 5;								// <int32: varint>
const int icehChangeObjectAttribute = 6;							// <std::string>

const int icehStreamHeader = 7;										// <std::string> filename
const int icehStreamBody = 8;										// <bytes>
const int icehStreamEnd = 9;										// <bool> true: ok, false: cancel
const int icehStreamID = 10;										// <id: bytes/std::string>

const unsigned char imbMagic = 0xFE;

const int imbMinimumPacketSize = 16;
const int imbMaximumPayloadSize = 10 * 1024 * 1024;
const int imbSocketDefaultLingerTimeInSec = 2;  // in sec

// command tags
const int icehRemark = 1;											// <string>
const int icehSubscribe = 2;										// <uint32: varint>
const int icehPublish = 3;											// <uint32: varint>
const int icehUnsubscribe = 4;										// <uint32: varint>
const int icehUnpublish = 5;										// <uint32: varint>
const int icehSetEventIDTranslation = 6;							// <uint32: varint>
const int icehEventName = 7;										// <std::string>
const int icehEventID = 8;											// <uint32: varint>

const int icehUniqueClientID = 11;									// <guid>
const int icehHubID = 12;											// <guid>
const int icehModelName = 13;										// <std::string>
const int icehModelID = 14;											// <int32: varint> ?
const int icehReconnectable = 15;									// <bool: varint>
const int icehState = 16;											// <uint32: varint>
const int icehEventNameFilter = 17;									// <std::string>
const int icehNoDelay = 18;											// <bool: varint>
const int icehClose = 21;											// <bool: varint>
const int icehReconnect = 22;										// <guid>

// imb return codes
const int iceConnectionClosed = -1;
const int iceNoHandler = -2;
const int iceEventNameFiltered = -3;
const int iceStoredEvent = 0;

const TEventID imbInvalidEventID = 0xFFFFFFFF;

// monitoring info tags
const int icehMonitoringBase = 1000;
// request initial state command
const int imfRequestInitialState = icehMonitoringBase + 0;			// <string:return-event-name>
// stats hub
const int imfHubStatsTime = icehMonitoringBase + 1;                 // <int64: varint>
const int imfHubStatsBytes = icehMonitoringBase + 2;                // <int32: varint>
const int imfHubStatsEvents = icehMonitoringBase + 3;               // <int32: varint>
// stats events
const int imfEventStatsEventID = icehMonitoringBase + 4;            // <TEventID: varint>
const int imfEventStatsEventsIn = icehMonitoringBase + 5;			// <int32: varint>
const int imfEventStatsEventsOut = icehMonitoringBase + 6;			// <int32: varint>
// stats connections
const int imfConStatsUniqueClientID = icehMonitoringBase + 7;       // <guid>
const int imfConStatsEvents = icehMonitoringBase + 8;				// <int32: varint>
const int imfConStatsPacketsDropped = icehMonitoringBase + 9;		// <int32: varint>
const int imfConStatsCommands = icehMonitoringBase + 10;			// <int32: varint>
const int imfConStatsHeartBeats = icehMonitoringBase + 11;			// <int32: varint>
const int imfConStatsBytesRead = icehMonitoringBase + 12;			// <uint64: varint>
const int imfConStatsBytesWritten = icehMonitoringBase + 13;		// <uint64: varint>
// log
const int imfLogEntry = icehMonitoringBase + 14;                    // <uint32: varint>
const int imfLogLine = icehMonitoringBase + 15;						// <string>
// stats listeners
const int imfListenerStatsID = icehMonitoringBase + 16;             // <guid>
const int imfListenerStatsState = icehMonitoringBase + 17;			// <string>
const int imfListenerStatsConnects = icehMonitoringBase + 18;		// <int32: varint>
// hub state
const int imfHubEventNameFilter = icehMonitoringBase + 30;          // <string>
// connection state
const int imfConnectionOpen = icehMonitoringBase + 31;              // <guid>
const int imfConnectionChange = icehMonitoringBase + 32;            // <guid>
const int imfConnectionClose = icehMonitoringBase + 33;             // <guid>
const int imfConnectionModelName = icehMonitoringBase + 34;			// <string>
const int imfConnectionModelID = icehMonitoringBase + 35;         	// <int32: varint>
const int imfConnectionReconnectable = icehMonitoringBase + 36;		// <bool: varint>
const int imfConnectionRemoteAddr = icehMonitoringBase + 37;		// <string>
const int imfConnectionState = icehMonitoringBase + 38;				// <uint32: varint>
const int imfConnectionNoDelay = icehMonitoringBase + 39;			// <bool: varint>
const int imfConnectionBuffering = icehMonitoringBase + 40;			// <bool: varint>
const int imfConnectionEventNameFilter = icehMonitoringBase + 41;	// <string>
const int imfConnectionListener = icehMonitoringBase + 42;			// <guid>
// event state
const int imfEventNew = icehMonitoringBase + 51;                    // <TEventID: varint>
const int imfEventUpdate = icehMonitoringBase + 52;                 // <TEventID: varint>
const int imfEventEnd = icehMonitoringBase + 53;                    // <TEventID: varint>
const int imfEventName = icehMonitoringBase + 54;					// <string>
const int imfEventSubscriber = icehMonitoringBase + 55;				// <guid>
const int imfEventNoSubscriber = icehMonitoringBase + 56;			// <guid>
const int imfEventPublisher = icehMonitoringBase + 57;				// <guid>
const int imfEventNoPublisher = icehMonitoringBase + 58;			// <guid>
const int imfEventParent = icehMonitoringBase + 59;					// <TEventID: varint>
const int imfEventNoParent = icehMonitoringBase + 60;				// <bool: varint> always false
// serial port state
const int imfSerialPortOpen = icehMonitoringBase + 71;              // <string>
const int imfSerialPortChange = icehMonitoringBase + 72;            // <string>
const int imfSerialPortClose = icehMonitoringBase + 73;             // <string>
const int imfSerialPortFriendlyName = icehMonitoringBase + 74;		// <string>
// listeners state
const int imfListenerAdd = icehMonitoringBase + 81;                 // <guid>
const int imfListenerRemove = icehMonitoringBase + 82;              // <guid>
const int imfListenerName = icehMonitoringBase + 83;				// <string>

// string utils
std::string toUpper(const std::string & s);
std::string intToString(int64_t aValue);
bool startsWith(const std::string & aValue, const std::string & aStart);
bool endsWith(const std::string & aValue, const std::string & aEnd);
std::string endStrip(const std::string & aValue, const std::string & aEnd);
bool compareStringCaseInsensitive(const std::string & aString1, const std::string & aString2);

// guid utils
GUID createGUID();
bool sameGUID(const GUID & aGUID1, const GUID & aGUID2);
bool isEmptyGUID(const GUID & aGUID);
void emptyGUID(GUID & aGUID);
GUID emptyGUID();
std::string GUIDToHex(const GUID & aGUID);

// helper for GUID to support using it being used in map
inline bool operator < (const GUID & Left, const GUID & Right) { return memcmp(&Left, &Right, sizeof(GUID)) < 0; };

enum TLogLevel {
	llRemark,
	llDump,
	llNormal,
	llStart,
	llFinish,
	llPush,
	llPop,
	llStamp,
	llSummary,
	llWarning,
	llError,
	llOK
};

// TByteBuffer

class TByteBuffer {
public:
	TByteBuffer(int aCapacity = 0);
	TByteBuffer(const byte* aBuffer, int aSize);
	TByteBuffer(const byte* aBuffer, int aOffset, int aLimit);
	TByteBuffer(const TByteBuffer & aBuffer);					// copy constructor
	TByteBuffer(TByteBuffer && aBuffer);						// move constructor
	~TByteBuffer(void);
public:
	TByteBuffer& operator=(const TByteBuffer & aBuffer);		// copy assignment
	TByteBuffer& operator=(TByteBuffer && aBuffer);				// move assignment
public:
	byte* getBuffer() { return fBuffer; };
	byte* getBuffer(int aOffset) { if (fBuffer != 0) return &fBuffer[aOffset]; else return 0; };
	int getCapacity() { return fCapacity; };
	void setCapacity(int aNewCapacity);
	int getOffset() { return fOffset; };
	void setOffset(int aValue) { if (aValue >= 0) { fOffset = aValue; if (fOffset > fCapacity) setCapacity(fOffset); } };
	int getLimit() { return fLimit; };
	void setLimit(int aValue) { if (aValue >= 0) { fLimit = aValue; if (fLimit > fCapacity) setCapacity(fLimit); } };
	byte getFirstByte() { return fBuffer[0]; };
public:
	int remaining() const { return fLimit - fOffset; };
	void shiftLeftOneByte(byte aRighbyte);
private:
	byte* fBuffer;
	int fCapacity;
	int fOffset;
	int fLimit;
public:
	uint64_t bb_read_uint64();									// unsigned varint
	int64_t bb_read_int64();									// signed varint
	uint32_t bb_read_uint32();									// unsigned varint
	int32_t bb_read_int32();									// signed varint
	bool bb_read_bool();										// 1 byte varint
	double bb_read_double();									// 64 bit float
	float bb_read_single();										// 32 bit float
	GUID bb_read_guid();										// length delimited
	std::string bb_read_string();								// length delimited
	TByteBuffer bb_read_bytes();								// length delimited

	void bb_read_skip(int aWiretype);
	void bb_read_skip_bytes(int aNumberOfBytes);

	// peek and replace do not forward offset in buffer
	void bb_peek_bytes(void* aValue, int aValueSize);
	void bb_replace_bytes(void* aValue, int aValueSize);
private:
	void bb_put_uint64(uint64_t aValue);
	void bb_put_uint32(uint32_t aValue);
	void bb_put_bytes(const byte* aValue, int aValueSize);
	void bb_put_bytes(const char* aValue, int aValueSize);
	static int bb_var_int_size(uint64_t aValue);
public:
	static TByteBuffer empty() { TByteBuffer bb(0); return bb; };
	static TByteBuffer join(const TByteBuffer aBuffers[], int aNumberOfBuffers);
	static TByteBuffer join(const TByteBuffer & aBuffer1, const TByteBuffer & aBuffer2);

	static TByteBuffer bb_bool(bool aValue);					// unsigned varint
	static TByteBuffer bb_byte(byte aValue);					// unsigned varint
	static TByteBuffer bb_uint64(uint64_t aValue);				// unsigned varint
	static TByteBuffer bb_uint32(uint32_t aValue);				// unsigned varint
	static TByteBuffer bb_int64(int64_t aValue);				// signed varint
	static TByteBuffer bb_int32(int32_t aValue);				// unsigned varint
	static TByteBuffer bb_single(float aValue);					// length delimited
	static TByteBuffer bb_double(double aValue);				// length delimited
	static TByteBuffer bb_bytes(byte* aValue, int aValueSize);	// length delimited
	static TByteBuffer bb_string(const std::string & aValue);	// length delimited
	static TByteBuffer bb_guid(const GUID & aValue);			// length delimited

	static TByteBuffer bb_tag_int32(uint32_t aTag, int32_t aValue);
	static TByteBuffer bb_tag_uint32(uint32_t aTag, uint32_t aValue);
	static TByteBuffer bb_tag_int64(uint32_t aTag, int64_t aValue);
	static TByteBuffer bb_tag_uint64(uint32_t aTag, uint64_t aValue);
	static TByteBuffer bb_tag_bool(uint32_t aTag, bool aValue);
	static TByteBuffer bb_tag_single(uint32_t aTag, float aValue);
	static TByteBuffer bb_tag_double(uint32_t aTag, double aValue);
	static TByteBuffer bb_tag_guid(uint32_t aTag, const GUID & aValue);
	static TByteBuffer bb_tag_string(uint32_t aTag, const std::string & aValue);
	static TByteBuffer bb_tag_bytes(uint32_t aTag, byte* aValue, int aValueSize);
};

class TByteBuffers {
public:
	TByteBuffers(int aBufferCount);
	~TByteBuffers();

	TByteBuffer* buffers = 0;
	int bufferCount = 0;
	TByteBuffer join();
	int remaining();
};

class TConnection; // forward

typedef std::set<TConnection*> TConnectionList;

class TEventEntry {
public:
	TEventEntry(const TEventEntry & aEventEntry); // deep copy
	TEventEntry(/*THub* aHub, */const std::string & aName, TEventID aID, TEventEntry* aParent) { /*fHub = aHub; */fID = aID; fName = aName; fParent = aParent; };
	virtual ~TEventEntry() { /*fHub = 0;*/ fParent = 0; };

	void handleEvent(TConnection* aConnection, TByteBuffer & aPacket);
	void handleConnectionFree(TConnection* aConnection);

	/*THub* getHub() { return fHub; };*/
	TEventID getID() { return fID; };
	std::string getName() { return fName; };
	void setName(const std::string & aName) { fName = aName; };
	TEventEntry* getParent() { return fParent; };
	void setParent(TEventEntry* aParent) { fParent = aParent; };
	bool isChild(TEventEntry* aParent);
	TConnectionList* getSubscribers() { return &fSubscribers; };
	TConnectionList* getPublishers() { return &fPublishers; };

	bool isEmpty() { return fSubscribers.size() == 0 && fPublishers.size() == 0; };
private:
	/*THub* fHub;*/
	TEventID fID;
	std::string fName;
	TEventEntry* fParent;
	TConnectionList fSubscribers;
	TConnectionList fPublishers;
	// stats
public:
	void resetStats();
	TByteBuffer BBStats();
	int getEventsIn() { return fEventsIn; };
	int getEventsOut() { return fEventsOut; };
private:
	std::atomic<int> fEventsIn;
	std::atomic<int> fEventsOut;
};

enum TConnectionState {
	icsUninitialized = 0,
	icsInitialized = 1,
	icsClient = 2,
	icsHub = 3,
	icsEnded = 4,
	//icsTimer = 10,
	icsGateway = 100
};

typedef std::unique_ptr<TEventEntry> TSmartEventEntryPtr;

class TConnection {
public:
	TConnection(/*THub* aHub, const GUID & aListener, const std::string & aRemoteAddress*/);
	virtual ~TConnection();

	virtual void startReaderThread();

	GUID getUniqueClientID() { return fUniqueClientID; };
	//GUID getListener() { return fListener; };
	//std::string getRemoteAddress() { return fRemoteAddress; };
	bool getReconnectable() { return fReconnectable; };
	std::string getEventNameFilter() { return fEventNameFilter; };
	std::string getModelName() { return fModelName; };
	int getModelID() { return fModelID; };
	TConnectionState getState() { return fState; };
	bool getNoDelay() { return fNoDelay; };
	bool isAnyReceiver() { return (fState == icsHub || fState == icsGateway); };
	bool isSubscribeReceiver(const std::string & aEventName) { return isAnyReceiver() && startsWith(aEventName, fEventNameFilter); };
	bool isPublishReceiver(const std::string & aEventName) { return isAnyReceiver() && startsWith(aEventName, fEventNameFilter); };
	bool isEventReceiver() { return fState == icsClient || fState == icsHub || fState == icsGateway; };
	void storeEventIDTranslation(TEventID aRemoteEventID, TEventID aLocalEventID);
	// to override
	virtual bool getConnected() = 0;
	virtual bool handleReconnect(TConnection* aConnection) { return false; }; // todo: = 0;
	virtual void handleStoredEvents() {}; // todo: = 0;
protected:
	GUID fUniqueClientID;
	/*THub* fHub;*/
	/*GUID fListener;*/
	/*std::string fRemoteAddress;*/
	std::thread fReaderThread;
	std::mutex fWriteLock;
	std::vector<TEventID> fRemoteToLocalEventID;
	bool fReconnectable = false;
	std::string fEventNameFilter = "";
	std::string fModelName = "undefined";
	int fModelID = 0;
	TConnectionState fState = icsUninitialized;
	bool fNoDelay = false;

	// to override
	virtual int readBytes(const byte* aBuffer, int aOffset, int aLimit) = 0;
	virtual int writeBytes(const byte* aBuffer, int aSize) = 0;

	// optional to override
	virtual void setNoDelay(bool aValue) { fNoDelay = aValue; };
	virtual void _close(bool aSendCloseCmd) {};

	void setState(TConnectionState aState) { if (fState != aState) { fState = aState; signalState(fState); } }

	virtual void readCommands();
	void handleCommand(TByteBuffer & aBuffer);
	void handleEvent(TByteBuffer & aBuffer);
public:
	int setup(const std::string & aModelName, int aModelID, TConnectionState aState, const std::string &aEventNameFilter, bool aReconnectable);
public:
	virtual int signalPacket(TByteBuffer & aPacket);
protected:
	int signalCommand(TByteBuffer aCommand);
	int signalCommands(TByteBuffer aCommands[], int aNumberOfCommands);
public:
	int signalClose();
	int signalState(TConnectionState aState);
	int signalPublish(TEventID aLocalEventID, const std::string & aEventName);
	int signalSubscribe(TEventID aLocalEventID, const std::string & aEventName);
	int signalUnPublish(TEventID aLocalEventID, const std::string & aEventName);
	int signalUnSubscribe(TEventID aLocalEventID, const std::string & aEventName);
	int signalConnect();
	int signalRemark(const std::string & aRemark);
	int signalEventTranslation(TEventID aRemoteEventID, TEventID aLocalEventID);
	// stats
public:
	int getPacketsDropped() { return fPacketsDropped.load(); };
	int getEvents() { return fEvents.load(); };
	int getCommands() { return fCommands.load(); };
	int getHeartBeats() { return fHeartBeats.load(); };
	uint64_t getBytesRead() { return fBytesRead.load(); };
	uint64_t getBytesWritten() { return fBytesWritten.load(); };

	void resetStats();
	TByteBuffer BBStats();
protected:
	std::atomic<int> fPacketsDropped;
	std::atomic<int> fEvents;
	std::atomic<int> fCommands;
	std::atomic<int> fHeartBeats;
	std::atomic<uint64_t> fBytesRead;
	std::atomic<uint64_t> fBytesWritten;
};

typedef std::unique_ptr<TConnection> TSmartConnectionPtr;



class TConnectionTCP : public TConnection {
public:
	TConnectionTCP(/*THub* aHub, const GUID & aListener*/) : TConnection(/*aHub, aListener, "TCP unconnected"*/) { fSocket = -1; }
	TConnectionTCP(/*THub* aHub, const GUID & aListener, */const std::string & aRemoteHost, const std::string & aRemotePort);// : TConnection(/*aHub, aListener, aRemoteAddress*/) { fSocket = aSocket; }
	virtual ~TConnectionTCP() { _close(false); } // call _close of this class
protected:
	std::string fRemoteHost;
	std::string fRemotePort;
	SOCKET fSocket;

	virtual int readBytes(const byte* aBuffer, int aOffset, int aLimit) {
		return recv(fSocket, (char*)&aBuffer[aOffset], aLimit - aOffset, MSG_WAITALL);
	}
	virtual int writeBytes(const byte* aBuffer, int aSize) {
		std::lock_guard<std::mutex> guard(fWriteLock);
		return send(fSocket, (char*)aBuffer, aSize, 0);
	}
	virtual bool getConnected() { return fSocket != -1; }
	virtual void _close(bool aSendCloseCmd) {
		//if (fSocket != -1) {
		//	close(fSocket);
		//	fSocket = -1;
		//}
		if (getConnected()) {
			if (aSendCloseCmd)
				signalClose();
			SOCKET _socket = fSocket;
			fSocket = -1;
			// todo: fHub->handleConnectionClose(this);
			if (_socket != -1) {
				// todo: shutdown first?
				close(_socket);
				//fSocket = -1;
			}
		}
	}
};

class TConnectionTLS : public TConnection {
public:
	TConnectionTLS(/*THub* aHub, const GUID & aListener*/) : TConnection(/*aHub, aListener, "TLS unconnected"*/) { fSocket = -1; fSSL = 0; }
	TConnectionTLS(/*THub* aHub, const GUID & aListener, */const std::string & aRemoteHost, const std::string & aRemotePort); // : TConnection(/*aHub, aListener, */aRemoteAddress) { fSocket = aSocket; fSSL = aSSL; }
	virtual ~TConnectionTLS() { _close(false); } // call _close of this class
	virtual std::string getRemoteAddress() { return "TLS " + fRemoteHost + ":" + fRemotePort; };
protected:
	std::string fRemoteHost;
	std::string fRemotePort;
	SOCKET fSocket;
	SSL* fSSL;
	
	virtual int readBytes(const byte* aBuffer, int aOffset, int aLimit) {
		return SSL_read(fSSL, (void*)&aBuffer[aOffset], aLimit - aOffset);
	}
	virtual int writeBytes(const byte* aBuffer, int aSize) {
		std::lock_guard<std::mutex> guard(fWriteLock);
		return SSL_write(fSSL, aBuffer, aSize);
	}
	virtual bool getConnected() { return fSocket != -1; }
	virtual void _close(bool aSendCloseCmd) {
		if (getConnected()) {
			if (aSendCloseCmd)
				signalClose();
			SOCKET _socket = fSocket;
			fSocket = -1;
			//fHub->handleConnectionClose(this);
			if (fSSL) {
				SSL_free(fSSL);
				fSSL = 0;
			}
			if (_socket != -1) {
				// todo: shutdown socket first?
				close(_socket);
				//fSocket = -1;
			}
		}
	}
};
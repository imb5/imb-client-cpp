#include <list>
#include <set>
#include <string>
#include <sstream>
#include <cstdint>
#include <iomanip>

// thread sleep
#include <thread>
#include <chrono>

// platform dependent code start
#ifdef __linux__

#include <string.h>

// install package uuid-dev
#include <uuid/uuid.h>

#elif _WIN32

#include <process.h>
#include <stdio.h>
#include <iostream>

#else

#error Platform not supported?

#endif
// platform dependent code end

#include "imb5.h"

// worm locks C++14: https://www.justsoftwaresolutions.co.uk/threading/new-concurrency-features-in-c++14.html

// high resolution time

uint64_t _startTime = uint64_t(time(nullptr)) * 10000000 + 116444736000000000;
auto _startTick = std::chrono::high_resolution_clock::now();

uint64_t NowUTCInt64() {
	// utc file time ie 100-nano seconds since 1601..
	// start time is low resolution (seconds)
	// diff is higher resolution, derived from high res time
	auto curTick = std::chrono::high_resolution_clock::now();
	return _startTime + std::chrono::duration_cast<std::chrono::milliseconds>(curTick - _startTick).count() * 10000;
}

// string utils

std::string toUpper(const std::string & aValue) {
	std::string ret(aValue.size(), char());
	for (unsigned int i = 0; i < aValue.size(); ++i)
		ret[i] = ('a' <= aValue[i] && aValue[i] <= 'z') ? aValue[i] - ('a' - 'A') : aValue[i];
	return ret;
}
bool startsWith(const std::string & aValue, const std::string & aStart) {
	// check for value >= start and for value and start being 0
	return aStart.size() <= aValue.size() ? (aStart.size() == 0 ? true : toUpper(aValue).substr(0, aStart.size()).compare(toUpper(aStart)) == 0) : false;
}
bool endsWith(const std::string & aValue, const std::string & aEnd) {
	return aEnd.size() <= aValue.size() ? (aEnd.size() == 0 ? true : toUpper(aValue).substr(aValue.size() - aEnd.size()).compare(toUpper(aEnd)) == 0) : false;
}
std::string endStrip(const std::string & aValue, const std::string & aEnd) {
	return endsWith(aValue, aEnd) ? aValue.substr(0, aValue.size() - aEnd.size()) : aValue;
}
bool compareStringCaseInsensitive(const std::string & aString1, const std::string & aString2) {
	if (aString1.size() != aString2.size())
		return false;
	for (std::string::const_iterator c1 = aString1.begin(), c2 = aString2.begin(); c1 != aString1.end(); ++c1, ++c2)
		if (*c1 != *c2) {
			if (*c1 >= 0 && *c2 >= 0) {
				if (tolower(*c1) != tolower(*c2))
					return false;
			}
			else
				return false;
		}
	return true;
}

// guid utils

GUID createGUID() {
	GUID guid;
#ifdef __linux__
	uuid_generate((unsigned char*)&guid);
#elif _WIN32
	HRESULT res = CoCreateGuid(&guid);
	if (res != S_OK)
		DEBUG(("## Could not create a valid guid in TConnection::TConnection: " + std::to_string(res)).c_str());
#endif
	return guid;
}
bool sameGUID(const GUID & aGUID1, const GUID & aGUID2) {
	return aGUID1.Data1 == 0 && aGUID1.Data2 == 0 && aGUID1.Data3 == 0 &&
		aGUID1.Data4[0] == 0 && aGUID1.Data4[1] == 0 && aGUID1.Data4[2] == 0 && aGUID1.Data4[3] == 0 &&
		aGUID1.Data4[4] == 0 && aGUID1.Data4[5] == 0 && aGUID1.Data4[6] == 0 && aGUID1.Data4[7] == 0;
}
bool isEmptyGUID(const GUID & aGUID) {
	return aGUID.Data1 == 0 && aGUID.Data2 == 0 && aGUID.Data3 == 0 &&
		aGUID.Data4[0] == 0 && aGUID.Data4[1] == 0 && aGUID.Data4[2] == 0 && aGUID.Data4[3] == 0 &&
		aGUID.Data4[4] == 0 && aGUID.Data4[5] == 0 && aGUID.Data4[6] == 0 && aGUID.Data4[7] == 0;
}
void emptyGUID(GUID & aGUID) {
	memset(&aGUID, 0, sizeof(GUID));
}
GUID emptyGUID() {
	GUID res;
	emptyGUID(res);
	return res;
}
std::string GUIDToHex(const GUID & aGUID) {
	std::ostringstream oss;
	oss << std::uppercase <<
		std::setfill('0') << std::setw(8) << std::hex << aGUID.Data1 <<
		std::setfill('0') << std::setw(4) << std::hex << aGUID.Data2 <<
		std::setfill('0') << std::setw(4) << std::hex << aGUID.Data3 <<
		std::setfill('0') << std::setw(2) << std::hex << (int)aGUID.Data4[0] <<
		std::setfill('0') << std::setw(2) << std::hex << (int)aGUID.Data4[1] <<
		std::setfill('0') << std::setw(2) << std::hex << (int)aGUID.Data4[2] <<
		std::setfill('0') << std::setw(2) << std::hex << (int)aGUID.Data4[3] <<
		std::setfill('0') << std::setw(2) << std::hex << (int)aGUID.Data4[4] <<
		std::setfill('0') << std::setw(2) << std::hex << (int)aGUID.Data4[5] <<
		std::setfill('0') << std::setw(2) << std::hex << (int)aGUID.Data4[6] <<
		std::setfill('0') << std::setw(2) << std::hex << (int)aGUID.Data4[7];
	return oss.str();
}

// socket/ssl utils

SOCKET connect_tcp_socket(const std::string & aHost, const std::string & aPort) {
	struct addrinfo hints, *res;
	SOCKET sockfd;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	getaddrinfo(aHost.c_str(), aPort.c_str(), &hints, &res);
	sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	connect(sockfd, res->ai_addr, (int)res->ai_addrlen);
	return sockfd;
}

SOCKET create_tcp_socket(int port) {
	SOCKET s;
	struct sockaddr_in addr;

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_ANY);
	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0) {
		perror("Unable to create socket");
		exit(EXIT_FAILURE);
	}
	if (bind(s, (struct sockaddr*)&addr, sizeof(addr)) < 0) {
		perror("Unable to bind");
		exit(EXIT_FAILURE);
	}
	if (listen(s, 1) < 0) {
		perror("Unable to listen");
		exit(EXIT_FAILURE);
	}
	return s;
}

SSL_CTX *create_context() {
	const SSL_METHOD *method;
	SSL_CTX *ctx;

	method = SSLv23_server_method();
	ctx = SSL_CTX_new(method);
	if (!ctx) {
		perror("Unable to create SSL context");
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}
	return ctx;
}

void configure_context(SSL_CTX *ctx) {
	SSL_CTX_set_ecdh_auto(ctx, 1);
	// set cert
	if (SSL_CTX_use_certificate_file(ctx, "client-eco-district.crt", SSL_FILETYPE_PEM) < 0) {
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}
	// set key (without passphrase)
	if (SSL_CTX_use_PrivateKey_file(ctx, "client-eco-district.nopass.key", SSL_FILETYPE_PEM) < 0) {
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}
	// add root certificate
	if (SSL_CTX_load_verify_locations(ctx, "root-ca-imb.crt", NULL) < 0) {
		ERR_print_errors_fp(stderr);
		exit(EXIT_FAILURE);
	}
}

// TByteBuffer

TByteBuffer::TByteBuffer(int aCapacity) {
	fOffset = 0;
	fLimit = 0;
	fCapacity = aCapacity;
	if (aCapacity > 0)
		fBuffer = new byte[aCapacity];
	else
		fBuffer = 0;
}
TByteBuffer::TByteBuffer(const byte* aBuffer, int aSize) {
	fOffset = 0;
	fLimit = aSize;
	fCapacity = aSize;
	if (aSize > 0) {
		fBuffer = new byte[aSize];
		if (aBuffer != 0)
			memcpy(fBuffer, aBuffer, aSize);
	}
	else
		fBuffer = 0;
}
TByteBuffer::TByteBuffer(const byte* aBuffer, int aOffset, int aLimit) {
	// define empty buffer
	fOffset = 0;
	fLimit = aLimit - aOffset;
	fCapacity = fLimit;
	if (fCapacity > 0) {
		fBuffer = new byte[fCapacity];
		if (aBuffer != 0)
			memcpy(fBuffer, &aBuffer[aOffset], fCapacity);
	}
	else
		fBuffer = 0;
}
TByteBuffer::TByteBuffer(const TByteBuffer & aBuffer) {
	// deep copy of TByteBuffer
	fOffset = aBuffer.fOffset;
	fLimit = aBuffer.fLimit;
	fCapacity = aBuffer.fCapacity;
	if (fCapacity > 0) {
		fBuffer = new byte[fCapacity];
		if (aBuffer.fBuffer != 0)
			memcpy(fBuffer, aBuffer.fBuffer, fCapacity);
	}
	else
		fBuffer = 0;
}
TByteBuffer::TByteBuffer(TByteBuffer && aBuffer) /*: fBuffer(nullptr), fCapacity(0), fOffset(0), fLimit(0)*/
{
	// move constructor
	fBuffer = aBuffer.fBuffer;
	fCapacity = aBuffer.fCapacity;
	fOffset = aBuffer.fOffset;
	fLimit = aBuffer.fLimit;
	aBuffer.fBuffer = nullptr;
	aBuffer.fCapacity = 0;
	aBuffer.fOffset = 0;
	aBuffer.fLimit = 0;
}
TByteBuffer::~TByteBuffer() {
	delete[] fBuffer;
	fBuffer = 0;
	fCapacity = 0;
	fLimit = 0;
	fOffset = 0;
}
TByteBuffer& TByteBuffer::operator = (const TByteBuffer & aBuffer) {
	// check for self-assignment
	if (this == &aBuffer)
		return *this;
	// check for existing buffer
	delete[] fBuffer;
	// deep copy
	fOffset = aBuffer.fOffset;
	fLimit = aBuffer.fLimit;
	fCapacity = aBuffer.fCapacity;
	if (fCapacity > 0) {
		fBuffer = new byte[fCapacity];
		if (aBuffer.fBuffer != 0)
			memcpy(fBuffer, aBuffer.fBuffer, fCapacity);
	}
	else
		fBuffer = 0;
	return *this;
}
TByteBuffer & TByteBuffer::operator=(TByteBuffer && aBuffer)
{
	// move assignment
	if (this != &aBuffer) {
		delete[] fBuffer;
		fBuffer = aBuffer.fBuffer;
		fCapacity = aBuffer.fCapacity;
		fOffset = aBuffer.fOffset;
		fLimit = aBuffer.fLimit;
		aBuffer.fBuffer = nullptr;
		aBuffer.fCapacity = 0;
		aBuffer.fOffset = 0;
		aBuffer.fLimit = 0;
	}
	return *this;
}
void TByteBuffer::setCapacity(int aNewCapacity) {
	if (aNewCapacity != fCapacity) {
		if (aNewCapacity != 0) {
			// create new buffer
			byte* newBuffer = new byte[aNewCapacity];
			// copy existing data
			if (fBuffer != 0) {
				if (fCapacity > 0) {
					// there is data to copy
					if (aNewCapacity > fCapacity) {
						memcpy(newBuffer, fBuffer, fCapacity);
						memset(&newBuffer[fCapacity], 0, aNewCapacity - fCapacity);
					}
					else
						memcpy(newBuffer, fBuffer, aNewCapacity);
				}
				// data copied -> delete old buffer
				delete[] fBuffer;
			}
			// switch to new buffer
			fBuffer = newBuffer;
			fCapacity = aNewCapacity;
		}
		else {
			delete[] fBuffer;
			fBuffer = 0;
			fCapacity = 0;
		}
		if (fOffset > fCapacity)
			fOffset = fCapacity;
		if (fLimit > fCapacity)
			fLimit = fCapacity;
	}
}
uint64_t TByteBuffer::bb_read_uint64() {
	// unsigned varint
	int shiftLeft = 0;
	uint64_t b = 0;
	uint64_t res = 0;
	do {
		if (fOffset >= fCapacity)
			throw std::out_of_range("tried to read outside buffered data in TByteBuffer::bb_read_uint64");
		b = fBuffer[fOffset++];
		res |= (b & 0x7F) << shiftLeft;
		shiftLeft += 7;
	} while (b >= 128);
	return res;
}
int64_t TByteBuffer::bb_read_int64() {
	// signed varint
	uint64_t ui64 = bb_read_uint64();
	// remove sign bit
	int64_t res = (int64_t)(ui64 >> 1);
	// adjust for negative
	if ((ui64 & 1) == 1)
		res = -(res + 1);
	return res;
}
uint32_t TByteBuffer::bb_read_uint32() {
	// unsigned varint
	int shiftLeft = 0;
	uint32_t b = 0;
	uint32_t res = 0;
	do {
		if (fOffset >= fCapacity)
			throw std::out_of_range("tried to read outside buffered data in TByteBuffer::bb_read_uint32");
		b = fBuffer[fOffset++];
		res |= (b & 0x7F) << shiftLeft;
		shiftLeft += 7;
	} while (b >= 128);
	return res;
}
int32_t TByteBuffer::bb_read_int32() {
	// signed varint
	uint32_t ui32 = bb_read_uint32();
	// remove sign bit
	int32_t res = (int32_t)(ui32 >> 1);
	// adjust for negative
	if ((ui32 & 1) == 1)
		res = -(res + 1);
	return res;
}
bool TByteBuffer::bb_read_bool() {
	// 1 byte varint
	if (fOffset >= fCapacity)
		throw std::out_of_range("tried to read outside buffered data in TByteBuffer::bb_read_bool");
	return fBuffer[fOffset++] != 0;
}
double TByteBuffer::bb_read_double() {
	// 64 bit float
	fOffset += 8;
	if (fOffset > fCapacity)
		throw std::out_of_range("tried to read outside buffered data in TByteBuffer::bb_read_double");
	return *(double *)&fBuffer[fOffset - 8];
}
float TByteBuffer::bb_read_single() {
	//  32 bit float
	fOffset += 4;
	if (fOffset > fCapacity)
		throw std::out_of_range("tried to read outside buffered data in TByteBuffer::bb_read_single");
	return *(float *)&fBuffer[fOffset - 4];
}
GUID TByteBuffer::bb_read_guid() {
	// length delimited
	uint32_t len = bb_read_uint32();
	fOffset += len;
	if (fOffset > fCapacity)
		throw std::out_of_range("tried to read outside buffered data in TByteBuffer::bb_read_guid");
	return *(GUID *)&fBuffer[fOffset - len];
}
std::string TByteBuffer::bb_read_string() {
	// length delimited
	uint32_t len = bb_read_uint32();
	fOffset += len;
	if (fOffset > fCapacity)
		throw std::out_of_range("tried to read outside buffered data in TByteBuffer::bb_read_string");
	return std::string((char *)&fBuffer[fOffset - len], len);
}
TByteBuffer TByteBuffer::bb_read_bytes() {
	// length delimited
	uint32_t len = bb_read_uint32();
	fOffset += len;
	if (fOffset > fCapacity)
		throw std::out_of_range("tried to read outside buffered data in TByteBuffer::bb_read_bytes");
	TByteBuffer bb(fBuffer, fOffset - len, fOffset);
	return bb;
}
void TByteBuffer::bb_read_skip(int aWiretype) {
	switch (aWiretype) {
	case wt32Bit:
		fOffset += 4;
		break;
	case wt64Bit:
		fOffset += 8;
		break;
	case wtLengthDelimited:
		fOffset += bb_read_uint32();
		break;
	case wtVarInt:
		bb_read_uint64();
		break;
	default:
		//throw std::exception("invalid wiretype in TByteBuffer::bb_read_skip" >> Formatter::to_str);
		throw std::runtime_error("invalid wiretype in TByteBuffer::bb_read_skip");
	}
	if (fOffset > fCapacity)
		throw std::out_of_range("skipped outside buffered data in TByteBuffer::bb_read_skip");
}
void TByteBuffer::bb_read_skip_bytes(int aNumberOfBytes) {
	fOffset += aNumberOfBytes;
	if (fOffset > fCapacity)
		throw std::out_of_range("skipped outside buffered data in TByteBuffer::bb_read_skip_bytes");
}
void TByteBuffer::bb_peek_bytes(void * aValue, int aValueSize) {
	if (fOffset + aValueSize > fCapacity)
		throw std::out_of_range("reading outside buffered data in TByteBuffer::bb_peek_bytes");
	memcpy(aValue, &fBuffer[fOffset], aValueSize);
}
void TByteBuffer::bb_replace_bytes(void* aValue, int aValueSize) {
	// does not move offset, just overwrite of bytes on current location
	if (fOffset + aValueSize > fCapacity)
		throw std::out_of_range("writing outside buffered data in TByteBuffer::bb_replace_bytes");
	memcpy(&fBuffer[fOffset], aValue, aValueSize);
}
int TByteBuffer::bb_var_int_size(uint64_t aValue) {
	// encode in blocks of 7 bits (high order bit of byte is signal that more bytes are to follow
	// encode lower numbers directly for speed
	if (aValue < 128)
		return 1;
	else if (aValue < 128 * 128)
		return 2;
	else if (aValue < 128 * 128 * 128)
		return 3;
	else {
		// 4 bytes or more: change to dynamic size detection
		int res = 4;
		aValue >>= 7 * 4;
		while (aValue > 0) {
			aValue >>= 7;
			res++;
		}
		return res;
	}
}
TByteBuffer TByteBuffer::join(const TByteBuffer aBuffers[], int aNumberOfBuffers) {
	// calc total capacity for parts
	int capacity = 0;
	for (int b = 0; b < aNumberOfBuffers; b++)
		capacity += aBuffers[b].remaining();
	// create buffer and fill with data from parts
	TByteBuffer bb(capacity);
	for (int b = 0; b < aNumberOfBuffers; b++) {
		memcpy(&bb.fBuffer[bb.fLimit], &aBuffers[b].fBuffer[aBuffers[b].fOffset], aBuffers[b].remaining());
		bb.fLimit += aBuffers[b].remaining();
	}
	return bb;
}
TByteBuffer TByteBuffer::join(const TByteBuffer & aBuffer1, const TByteBuffer & aBuffer2) {
	// calc total capacity for parts
	int rem1 = aBuffer1.fLimit - aBuffer1.fOffset;
	int rem2 = aBuffer2.fLimit - aBuffer2.fOffset;
	int capacity = rem1 + rem2;
	// create buffer and fill with data from parts
	TByteBuffer bb(capacity);
	memcpy(&bb.fBuffer[bb.fLimit], &aBuffer1.fBuffer[aBuffer1.fOffset], rem1);
	bb.fLimit += rem1;
	memcpy(&bb.fBuffer[bb.fLimit], &aBuffer2.fBuffer[aBuffer2.fOffset], rem2);
	bb.fLimit += rem2;
	return bb;
}
void TByteBuffer::bb_put_uint64(uint64_t aValue) {
	while (aValue >= 128) {
		if (fLimit >= fCapacity)
			throw std::out_of_range("writing outside buffered data in TByteBuffer::bb_put_uint64");
		fBuffer[fLimit++] = (byte)((aValue & 0x7F) | 0x80); // msb: signal more bytes are to follow
		aValue >>= 7;
	}
	fBuffer[fLimit++] = (byte)aValue; // aValue<128 (msb already 0)
}
void TByteBuffer::bb_put_uint32(uint32_t aValue) {
	while (aValue >= 128) {
		if (fLimit >= fCapacity)
			throw std::out_of_range("writing outside buffered data in TByteBuffer::bb_put_uint32");
		fBuffer[fLimit++] = (byte)((aValue & 0x7F) | 0x80); // msb: signal more bytes are to follow
		aValue >>= 7;
	}
	fBuffer[fLimit++] = (byte)aValue; // aValue<128 (msb already 0)
}
void TByteBuffer::bb_put_bytes(const byte* aValue, int aValueSize) {
	if (fLimit + aValueSize > fCapacity)
		throw std::out_of_range("writing outside buffered data in TByteBuffer::bb_put_bytes(byte)");
	memcpy(&fBuffer[fLimit], aValue, aValueSize);
	fLimit += aValueSize;
}
void TByteBuffer::bb_put_bytes(const char* aValue, int aValueSize) {
	if (fLimit + aValueSize > fCapacity)
		throw std::out_of_range("writing outside buffered data in TByteBuffer::bb_put_bytes(char)");
	memcpy(&fBuffer[fLimit], aValue, aValueSize);
	fLimit += aValueSize;
}
TByteBuffer TByteBuffer::bb_bool(bool aValue) {
	// unsigned varint
	TByteBuffer bb(1);
	bb.fBuffer[0] = (byte)aValue;
	bb.fLimit += 1;
	return bb;
}
TByteBuffer TByteBuffer::bb_byte(byte aValue) {
	// unsigned varint
	TByteBuffer bb(1);
	bb.fBuffer[0] = aValue;
	bb.fLimit += 1;
	return bb;
}
TByteBuffer TByteBuffer::bb_uint64(uint64_t aValue) {
	// unsigned varint
	int len = TByteBuffer::bb_var_int_size(aValue);
	TByteBuffer bb(len);
	bb.bb_put_uint64(aValue);
	return bb;
}
TByteBuffer TByteBuffer::bb_uint32(uint32_t aValue) {
	// unsigned varint
	int len = TByteBuffer::bb_var_int_size(aValue);
	TByteBuffer bb(len);
	bb.bb_put_uint32(aValue);
	return bb;
}
TByteBuffer TByteBuffer::bb_int64(int64_t aValue) {
	// signed varint
	if (aValue < 0)
		return bb_uint64(((uint64_t)(-(aValue + 1)) << 1) | 1);
	else
		return bb_uint64((uint64_t)aValue << 1);
}
TByteBuffer TByteBuffer::bb_int32(int32_t aValue) {
	// unsigned varint
	if (aValue < 0)
		return bb_uint32(((uint32_t)(-(aValue + 1)) << 1) | 1);
	else
		return bb_uint32((uint32_t)aValue << 1);
}
TByteBuffer TByteBuffer::bb_single(float aValue) {
	// 32 bits, 4 bytes
	TByteBuffer bb(4);
	memcpy(bb.fBuffer, &aValue, 4);
	bb.fLimit += 4;
	return bb;
}
TByteBuffer TByteBuffer::bb_double(double aValue) {
	// 64 bits, 8 bytes
	TByteBuffer bb(8);
	memcpy(bb.fBuffer, &aValue, 8);
	bb.fLimit += 8;
	return bb;
}
TByteBuffer TByteBuffer::bb_bytes(byte* aValue, int aValueSize) {
	// length delimited
	TByteBuffer bb(TByteBuffer::bb_var_int_size(aValueSize) + aValueSize);
	bb.bb_put_uint64(aValueSize);
	bb.bb_put_bytes(aValue, aValueSize);
	return bb;
}
TByteBuffer TByteBuffer::bb_string(const std::string & aValue) {
	// length delimited
	size_t valueSize = aValue.size();
	TByteBuffer bb(TByteBuffer::bb_var_int_size(valueSize) + (int)valueSize);
	bb.bb_put_uint64(valueSize);
	bb.bb_put_bytes(aValue.data(), (int)valueSize);
	return bb;
}
TByteBuffer TByteBuffer::bb_guid(const GUID & aValue) {
	// length delimited
	int valueSize = sizeof(GUID);
	TByteBuffer bb(TByteBuffer::bb_var_int_size(valueSize) + valueSize);
	bb.bb_put_uint64(valueSize);
	bb.bb_put_bytes((byte*)&aValue, valueSize);
	return bb;
}
TByteBuffer TByteBuffer::bb_tag_int32(uint32_t aTag, int32_t aValue) {
	return TByteBuffer::join(
		TByteBuffer::bb_uint32((aTag << 3) | wtVarInt),
		TByteBuffer::bb_int32(aValue));
}
TByteBuffer TByteBuffer::bb_tag_uint32(uint32_t aTag, uint32_t aValue) {
	return TByteBuffer::join(
		TByteBuffer::bb_uint32((aTag << 3) | wtVarInt),
		TByteBuffer::bb_uint32(aValue));
}
TByteBuffer TByteBuffer::bb_tag_int64(uint32_t aTag, int64_t aValue) {
	return TByteBuffer::join(
		TByteBuffer::bb_uint32((aTag << 3) | wtVarInt),
		TByteBuffer::bb_int64(aValue));
}
TByteBuffer TByteBuffer::bb_tag_uint64(uint32_t aTag, uint64_t aValue) {
	return TByteBuffer::join(
		TByteBuffer::bb_uint32((aTag << 3) | wtVarInt),
		TByteBuffer::bb_uint64(aValue));
}
TByteBuffer TByteBuffer::bb_tag_bool(uint32_t aTag, bool aValue) {
	return TByteBuffer::join(
		TByteBuffer::bb_uint32((aTag << 3) | wtVarInt),
		TByteBuffer::bb_bool(aValue));
}
TByteBuffer TByteBuffer::bb_tag_single(uint32_t aTag, float aValue) {
	return TByteBuffer::join(
		TByteBuffer::bb_uint32((aTag << 3) | wt32Bit),
		TByteBuffer::bb_single(aValue));
}
TByteBuffer TByteBuffer::bb_tag_double(uint32_t aTag, double aValue) {
	return TByteBuffer::join(
		TByteBuffer::bb_uint32((aTag << 3) | wt64Bit),
		TByteBuffer::bb_double(aValue));
}
TByteBuffer TByteBuffer::bb_tag_guid(uint32_t aTag, const GUID & aValue) {
	return TByteBuffer::join(
		TByteBuffer::bb_uint32((aTag << 3) | wtLengthDelimited),
		TByteBuffer::bb_guid(aValue));
}
TByteBuffer TByteBuffer::bb_tag_string(uint32_t aTag, const std::string & aValue) {
	return TByteBuffer::join(
		TByteBuffer::bb_uint32((aTag << 3) | wtLengthDelimited),
		TByteBuffer::bb_string(aValue));
}
TByteBuffer TByteBuffer::bb_tag_bytes(uint32_t aTag, byte* aValue, int aValueSize) {
	return TByteBuffer::join(
		TByteBuffer::bb_uint32((aTag << 3) | wtLengthDelimited),
		TByteBuffer::bb_bytes(aValue, aValueSize));
}
void TByteBuffer::shiftLeftOneByte(byte aRightByte) {
	int rem = remaining();
	if (rem > 0) {
		if (rem > 1)
			memcpy(&fBuffer[fOffset], &fBuffer[fOffset + 1], rem - 1);
		fBuffer[fLimit - 1] = aRightByte;
	}
}

// TByteBuffers
// combine multiple TByteBuffer instances

TByteBuffers::TByteBuffers(int aBufferCount) {
	bufferCount = aBufferCount;
	if (bufferCount > 0)
		buffers = new TByteBuffer[aBufferCount];
	else
		buffers = 0;
}
TByteBuffers::~TByteBuffers() {
	if (bufferCount > 0) {
		delete[]buffers;
		buffers = 0;
		bufferCount = 0;
	}
}
TByteBuffer TByteBuffers::join() {
	return TByteBuffer::join(buffers, bufferCount);
}
int TByteBuffers::remaining() {
	int s = 0;
	for (int b = 0; b < bufferCount; b++)
		s += buffers[b].remaining();
	return s;
}

// TEventEntry

TEventEntry::TEventEntry(const TEventEntry & aEventEntry) {
	// deep copy
	/*fHub = aEventEntry.fHub;*/
	fID = aEventEntry.fID;
	fName = aEventEntry.fName;
	fParent = aEventEntry.fParent;
	fSubscribers = aEventEntry.fSubscribers;
	fPublishers = aEventEntry.fPublishers;
	fEventsIn.store(aEventEntry.fEventsIn.load());
	fEventsOut.store(aEventEntry.fEventsOut.load());
}
void TEventEntry::handleEvent(TConnection * aConnection, TByteBuffer & aPacket) {
	for (auto subscriber : fSubscribers) {
		if (subscriber != aConnection && subscriber->isEventReceiver()) {
			subscriber->signalPacket(aPacket);
			fEventsOut++;
		}
	}
	fEventsIn++;
}
void TEventEntry::handleConnectionFree(TConnection * aConnection) {
	size_t pe = fPublishers.erase(aConnection);
	size_t se = fSubscribers.erase(aConnection);
	/*
	if (pe > 0 || se > 0) {
		if (isEmpty())
			fHub->handleEventEnd(this);
		else
			fHub->handleEventRemoveConnection(this, aConnection, pe > 0, se > 0);
	}
	*/
}
bool TEventEntry::isChild(TEventEntry * aParent) {
	TEventEntry* e = this;
	while (e != 0 && e->fParent != aParent)
		e = e->fParent;
	return e != 0;
}
TByteBuffer TEventEntry::BBStats() {
	TByteBuffer stats[3]{
		TByteBuffer::bb_tag_uint32(imfEventStatsEventID, fID),
		TByteBuffer::bb_tag_int32(imfEventStatsEventsIn, fEventsIn.load()),
		TByteBuffer::bb_tag_int32(imfEventStatsEventsOut, fEventsOut.load())
	};
	return TByteBuffer::join(stats, 3);
}
void TEventEntry::resetStats() {
	fEventsIn.store(0);
	fEventsOut.store(0);
}

// TConnection

void TConnection::resetStats() {
	fEvents.store(0);
	fPacketsDropped.store(0);
	fCommands.store(0);
	fHeartBeats.store(0);
	fBytesRead.store(0);
	fBytesWritten.store(0);
}

TByteBuffer TConnection::BBStats() {
	TByteBuffer stats[7]{
		TByteBuffer::bb_tag_guid(imfConStatsUniqueClientID, fUniqueClientID),
		TByteBuffer::bb_tag_int32(imfConStatsEvents, fEvents.load()),
		TByteBuffer::bb_tag_int32(imfConStatsPacketsDropped, fPacketsDropped.load()),
		TByteBuffer::bb_tag_int32(imfConStatsCommands, fCommands.load()),
		TByteBuffer::bb_tag_int32(imfConStatsHeartBeats, fHeartBeats.load()),
		TByteBuffer::bb_tag_uint64(imfConStatsBytesRead, fBytesRead.load()),
		TByteBuffer::bb_tag_uint64(imfConStatsBytesWritten, fBytesWritten.load())
	};
	return TByteBuffer::join(stats, 7);
}

TConnection::TConnection(/*THub * aHub, const GUID & aListener, const std::string & aRemoteAddress*/) {
	fUniqueClientID = createGUID();
	/*fHub = aHub;*/ //  ref
	/*fListener = aListener;*/
	/*fRemoteAddress = aRemoteAddress;*/
}

TConnection::~TConnection() {
	_close(true);
	if (fReaderThread.joinable())
		fReaderThread.join();
}

void TConnection::storeEventIDTranslation(TEventID aRemoteEventID, TEventID aLocalEventID) {
	// make room
	while (aRemoteEventID >= fRemoteToLocalEventID.size())
		fRemoteToLocalEventID.push_back(imbInvalidEventID);
	// store (overwrite)
	fRemoteToLocalEventID[aRemoteEventID] = aLocalEventID;
}

void TConnection::readCommands() {
	// read from connection and process
	TByteBuffer packet(imbMinimumPacketSize);
	do {
		try {
			packet.setOffset(0);
			packet.setLimit(imbMinimumPacketSize);
			int receivedBytes = readBytes(packet.getBuffer(), packet.getOffset(), packet.getLimit());
			if (receivedBytes == imbMinimumPacketSize) {
				while (packet.getFirstByte() != imbMagic) {
					byte oneByte[1];
					if (readBytes(oneByte, 0, 1) == 1)
						packet.shiftLeftOneByte(oneByte[0]);
					else {
						_close(false);
						break;
					}
				}
				packet.bb_read_skip_bytes(1);
				// we have magic at the first byte
				int size = (int)packet.bb_read_int64();
				int absSize = abs(size);
				// check for too large packets
				if (absSize < imbMaximumPayloadSize) {
					int extraBytesOffset = packet.getLimit();
					packet.setLimit(packet.getOffset() + absSize);
					int extraBytesNeeded = packet.getLimit() - extraBytesOffset;
					if (extraBytesNeeded > 0) {

						receivedBytes = readBytes(packet.getBuffer(), extraBytesOffset, packet.getLimit());
						if (receivedBytes != extraBytesNeeded) {
							_close(false);
							break;
						}
					}
					// positive size means an event otherwise it is defined as a command
					if (size > 0)
						handleEvent(packet);
					else
						handleCommand(packet);
				}
				else {
					fPacketsDropped++;
					throw std::out_of_range("packet size to large in TConnection::readCommands, dropping");
				}
			}
			else {
				_close(false);
				break;
			}
		}
		catch (std::exception & e) {
			if (getConnected())
				DEBUG(e.what());
		}
	} while (getConnected());
}
void TConnection::handleCommand(TByteBuffer & aBuffer) {
	std::string eventName = "";
	uint32_t localEventID = imbInvalidEventID;
	//uint32_t remoteEventID;
	//GUID uniqueClientID;
	std::string eventNameFilter;
	// process tags
	while (aBuffer.remaining() > 0) {
		uint32_t fieldInfo = aBuffer.bb_read_uint32();
		// todo: ref hub
		/*
		switch (fieldInfo) {
		case (icehSubscribe << 3) | wtVarInt:
			remoteEventID = aBuffer.bb_read_uint32();
			if (startsWith(eventName, fEventNameFilter))
				fHub->handleSubscribe(this, remoteEventID, eventName);
			fCommands++;
			break;
		case (icehPublish << 3) | wtVarInt:
			remoteEventID = aBuffer.bb_read_uint32();
			if (startsWith(eventName, fEventNameFilter))
				fHub->handlePublish(this, remoteEventID, eventName);
			fCommands++;
			break;
		case (icehUnsubscribe << 3) | wtVarInt:
			remoteEventID = aBuffer.bb_read_uint32();
			// only process if known event id
			localEventID = remoteEventID < fRemoteToLocalEventID.size() ? fRemoteToLocalEventID[remoteEventID] : imbInvalidEventID;
			if (localEventID != imbInvalidEventID)
				fHub->handleUnSubscribe(this, localEventID, remoteEventID);
			fCommands++;
			break;
		case (icehUnpublish << 3) | wtVarInt:
			remoteEventID = aBuffer.bb_read_uint32();
			// only process if known event id
			localEventID = remoteEventID < fRemoteToLocalEventID.size() ? fRemoteToLocalEventID[remoteEventID] : imbInvalidEventID;
			if (localEventID != imbInvalidEventID)
				fHub->handleUnPublish(this, localEventID, remoteEventID);
			fCommands++;
			break;
		case (icehEventName << 3) | wtLengthDelimited:
			eventName = aBuffer.bb_read_string();
			break;
		case (icehEventID << 3) | wtVarInt:
			localEventID = aBuffer.bb_read_uint32();
			break;
		case (icehSetEventIDTranslation << 3) | wtVarInt:
			remoteEventID = aBuffer.bb_read_uint32();
			// cleanup invalid entries at end of list
			while (fRemoteToLocalEventID.size() > 0 && fRemoteToLocalEventID.back() == imbInvalidEventID)
				fRemoteToLocalEventID.pop_back();
			fCommands++;
			break;
		case (icehNoDelay << 3) | wtVarInt:
			setNoDelay(aBuffer.bb_read_bool());
			fHub->handleConnectionNoDelay(this);
			fCommands++;
			break;
		case (icehState << 3) | wtVarInt:
			fState = (TConnectionState)aBuffer.bb_read_uint32();
			fHub->handleConnectionState(this);
			fCommands++;
			break;
		case (icehClose << 3) | wtVarInt:
			aBuffer.bb_read_bool(); // dummy
			_close(false);
			fCommands++;
			break;
		case (icehReconnect << 3) | wtLengthDelimited:
			uniqueClientID = aBuffer.bb_read_guid();
			fHub->handleConnectionReconnect(this, uniqueClientID);
			fCommands++;
			break;
		case (icehModelName << 3) | wtLengthDelimited:
			fModelName = aBuffer.bb_read_string();
			fCommands++;
			break;
		case (icehModelID << 3) | wtVarInt:
			fModelID = aBuffer.bb_read_int32();
			fCommands++;
			break;
		case (icehReconnectable << 3) | wtVarInt:
			fReconnectable = aBuffer.bb_read_bool();
			fCommands++;
			break;
		case (icehEventNameFilter << 3) | wtLengthDelimited:
			eventNameFilter = aBuffer.bb_read_string();
			// only accept when more specific then given filter, can never return to less specific!
			if (startsWith(eventNameFilter, fEventNameFilter)) {
				if (!compareStringCaseInsensitive(eventNameFilter, fEventNameFilter)) {
					DEBUG((getRemoteAddress() + ": using more specific event name filter " + fEventNameFilter + " -> " + eventNameFilter).c_str());
					fEventNameFilter = eventNameFilter;
				}
				else
					DEBUG((getRemoteAddress() + ": using SAME event name filter " + fEventNameFilter + " -> " + eventNameFilter).c_str());
			}
			else
				DEBUG((getRemoteAddress() + ": ignoring less specific event name filter " + fEventNameFilter + " -> " + eventNameFilter).c_str());
			fCommands++;
			break;
		case (icehUniqueClientID << 3) | wtLengthDelimited:
			uniqueClientID = aBuffer.bb_read_guid();
			if (isEmptyGUID(uniqueClientID))
				signalConnect();
			else
				fUniqueClientID = uniqueClientID;
			fHub->handleConnectionConnect(this);
			fCommands++;
			break;
		default:
			aBuffer.bb_read_skip((int)fieldInfo & 7);
			break;
		}
		*/
	}
}
void TConnection::handleEvent(TByteBuffer & aBuffer) {
	fEvents++;
	// read remote event id and lookup local event id
	// event id is in packet as 16 bit unsigned int ie fixed 2 bytes to be able to replace given id with local id without resizing packet
	// 16 bit should be enough for any normal use scenario (65536 active events)
	TEventIDFixed remoteEventID;
	aBuffer.bb_peek_bytes(&remoteEventID, sizeof(remoteEventID));
	TEventID localEventID;
	if (remoteEventID < fRemoteToLocalEventID.size()) {
		localEventID = fRemoteToLocalEventID[remoteEventID];
		// replace remote event id by local event id
		if (localEventID != imbInvalidEventID) {
			TEventIDFixed localEventIDFixed = localEventID;
			aBuffer.bb_replace_bytes(&localEventIDFixed, sizeof(localEventIDFixed));
			/*fHub->handleEvent(this, aBuffer, localEventID);*/
		}
		else
			throw std::runtime_error("invalid event id in TConnection::handleEvent");
	}
	else
		throw std::out_of_range("event id outside set of known remote event ids in TConnection::handleEvent");
}
int TConnection::setup(const std::string & aModelName, int aModelID, TConnectionState aState, const std::string &aEventNameFilter, bool aReconnectable)
{
	emptyGUID(fUniqueClientID);
	fModelName = aModelName;
	fModelID = aModelID;
	fState = aState;
	fReconnectable = aReconnectable;
	fEventNameFilter = aEventNameFilter;
	TByteBuffer commands[6] = {
		TByteBuffer::bb_tag_string(icehModelName, fModelName),
		TByteBuffer::bb_tag_int32(icehModelID, fModelID),
		TByteBuffer::bb_tag_uint32(icehState, fState),
		TByteBuffer::bb_tag_bool(icehReconnectable, fReconnectable),
		TByteBuffer::bb_tag_string(icehEventNameFilter, fEventNameFilter),
		TByteBuffer::bb_tag_guid(icehUniqueClientID, fUniqueClientID) // trigger
	};
	return signalCommands(commands, 4);

}
void TConnection::startReaderThread() {
	fReaderThread = std::thread(&TConnection::readCommands, this);
}
int TConnection::signalPacket(TByteBuffer & aPacket) {
	if (getConnected()) {
		// packet starts at offset 0! (ignoring offset..)
		// check minimum packet size
		if (aPacket.getLimit() < imbMinimumPacketSize)
			aPacket.setLimit(imbMinimumPacketSize);
		// update stats
		fBytesWritten += aPacket.getLimit();
		// do the writing
		return writeBytes(aPacket.getBuffer(), aPacket.getLimit());
	}
	else
		return iceConnectionClosed;
}
int TConnection::signalCommand(TByteBuffer aCommand) {
	TByteBuffer packet[3] = {
		TByteBuffer::bb_byte(imbMagic),
		TByteBuffer::bb_int64(-aCommand.remaining()),
		aCommand
	};
	auto joinedPacket = TByteBuffer::join(packet, 3);
	return signalPacket(joinedPacket);
}
int TConnection::signalCommands(TByteBuffer aCommands[], int aNumberOfCommands) {
	TByteBuffers packet(1 + 1 + aNumberOfCommands);
	int64_t s = 0;
	for (int c = 0; c < aNumberOfCommands; c++) {
		packet.buffers[2 + c] = aCommands[c];
		s += aCommands[c].remaining();
	}
	packet.buffers[0] = TByteBuffer::bb_byte(imbMagic);
	packet.buffers[1] = TByteBuffer::bb_int64(-s);
	auto joinedPacket = packet.join();
	return signalPacket(joinedPacket);
}
int TConnection::signalClose() {
	return signalCommand(TByteBuffer::bb_tag_bool(icehClose, false));
}
int TConnection::signalState(TConnectionState aState) {
	return signalCommand(TByteBuffer::bb_tag_uint32(icehState, fState));
}
int TConnection::signalPublish(TEventID aLocalEventID, const std::string & aEventName) {
	if (startsWith(aEventName, fEventNameFilter)) {
		TByteBuffer commands[2] = {
			TByteBuffer::bb_tag_string(icehEventName, aEventName),
			TByteBuffer::bb_tag_uint32(icehPublish, aLocalEventID) };
		return signalCommands(commands, 2);
	}
	else
		return iceEventNameFiltered;
}
int TConnection::signalSubscribe(TEventID aLocalEventID, const std::string & aEventName) {
	if (startsWith(aEventName, fEventNameFilter)) {
		TByteBuffer commands[2] = {
			TByteBuffer::bb_tag_string(icehEventName, aEventName),
			TByteBuffer::bb_tag_uint32(icehSubscribe, aLocalEventID) };
		return signalCommands(commands, 2);
	}
	else
		return iceEventNameFiltered;
}
int TConnection::signalUnPublish(TEventID aLocalEventID, const std::string & aEventName) {
	if (startsWith(aEventName, fEventNameFilter))
		return signalCommand(TByteBuffer::bb_tag_uint32(icehUnpublish, aLocalEventID));
	else
		return iceEventNameFiltered;
}
int TConnection::signalUnSubscribe(TEventID aLocalEventID, const std::string & aEventName) {
	if (startsWith(aEventName, fEventNameFilter))
		return signalCommand(TByteBuffer::bb_tag_uint32(icehUnsubscribe, aLocalEventID));
	else
		return iceEventNameFiltered;
}
int TConnection::signalConnect() {
	TByteBuffer commands[5] = {
		TByteBuffer::bb_tag_uint32(icehState, fState),
		TByteBuffer::bb_tag_bool(icehReconnectable, fReconnectable),
		TByteBuffer::bb_tag_string(icehEventNameFilter, fEventNameFilter),
		/*TByteBuffer::bb_tag_guid(icehHubID, fHub->getMonitor()->getUniqueClientID()),*/
		TByteBuffer::bb_tag_guid(icehUniqueClientID, fUniqueClientID)
	};
	return signalCommands(commands, 5);
}
int TConnection::signalRemark(const std::string & aRemark) {
	return signalCommand(TByteBuffer::bb_tag_string(icehRemark, aRemark));
}
int TConnection::signalEventTranslation(TEventID aRemoteEventID, TEventID aLocalEventID) {
	TByteBuffer commands[2] = {
		TByteBuffer::bb_tag_uint32(icehEventID, aRemoteEventID),
		TByteBuffer::bb_tag_uint32(icehSetEventIDTranslation, aLocalEventID)
	};
	return signalCommands(commands, 2);
}

TConnectionTCP::TConnectionTCP(const std::string & aRemoteHost, const std::string & aRemotePort) : TConnection()
{
	fRemoteHost = aRemoteHost;
	fRemotePort = aRemotePort;
	fSocket = connect_tcp_socket(aRemoteHost, aRemotePort);
	// todo:
}

TConnectionTLS::TConnectionTLS(const std::string & aRemoteHost, const std::string & aRemotePort) : TConnection()
{
	fRemoteHost = aRemoteHost;
	fRemotePort = aRemotePort;
	fSocket = connect_tcp_socket(aRemoteHost, aRemotePort);
	fSSL = SSL_new(sslInitialization.CTX());
	SSL_set_fd(fSSL, (int)fSocket);
	// todo:

	// check, below code is from other example..
	// https://www.cs.utah.edu/~swalton/listings/articles/ssl_client.c

	if (SSL_connect(fSSL) != 1)
		;//BIO_printf(outbio, "Error: Could not build a SSL session to: %s.\n", dest_url);
	auto cert = SSL_get_peer_certificate(fSSL);
	if (cert == NULL)
		;// BIO_printf(outbio, "Error: Could not get a certificate from: %s.\n", dest_url);
	else
		;// BIO_printf(outbio, "Retrieved the server's certificate from: %s.\n", dest_url);
	/* ---------------------------------------------------------- *
   * extract various certificate information                    *
   * -----------------------------------------------------------*/
	//auto certname = X509_NAME_new();
	//auto certname = X509_get_subject_name(cert);

	/* ---------------------------------------------------------- *
	 * display the cert subject here                              *
	 * -----------------------------------------------------------*/
	//BIO_printf(outbio, "Displaying the certificate subject data:\n");
	//X509_NAME_print_ex(outbio, certname, 0, 0);
	//BIO_printf(outbio, "\n");

	/* ---------------------------------------------------------- *
	 * Free the structures we don't need anymore                  *
	 * -----------------------------------------------------------*/
	//X509_free(cert);

}